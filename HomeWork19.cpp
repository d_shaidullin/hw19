﻿#include <iostream>
#include <cstdlib>
#include<string>
using namespace std;


class Animal
{
public:
	virtual void Voice()
	{
		sound = "I am an animal";
		cout << sound;
		cout << endl;
	}

protected:
	string sound;
};


class Dog : public Animal
{
public:
	void Voice() override
	{
		sound = "Woof";
		cout << sound;
		cout << endl;
	}
};

class Cat:public Animal
{
public:
	void Voice() override
	{
		sound = "Meoow";
		cout << sound;
		cout << endl;
	}
};



class Human :public Animal
{
public:
	Human(){}
	Human(string _name): name(_name){}
	void Voice() override
	{
		sound = "Hello, my name is ";
		cout << sound << name << endl;
	}
private:
	string name;
};

int main()
{
	Cat mur,  gur;
	Dog dirt, sirt, pirt;
	Human Ivan("Ivan"), Guram("Guram");

	Animal* Animals[] = {&mur,&gur,&dirt,&sirt,&pirt,&Ivan,&Guram };
	

	for (int i = 0; i < 7; i++)
	{
		Animals[i]->Voice();
	}

}
